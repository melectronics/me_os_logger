/*
 * proto.c
 *
 *  Created on: Feb 27, 2014
 *      Author: John
 */


#include <string.h>
#include <main.h>
#include <comms/me_exch_proto.h>
#include <comms/me_exch_proto_tables.h>
#include <comms/me_exch_proto_drivers.h>
#include <comms/me_exch_proto_reporting.h>
#include <comms/me_exch_proto_sys.h>
#include <comms/me_exch_proto_datalog.h>
#include <comms/me_exch_proto_fw_upd.h>
#ifdef USB_PROTO
#include <usb_descriptor.h>
#include <usb_cdc.h>
#include <CDC1.h>
#else
#include <UART_Comms.h>
#endif
#include <lib/queue.h>
#include <lib/crc16.h>
#include <debug/debug.h>

#define MODULE_NAME		"COMMS"

/******************************************************************************/
//					Global Project Data
/******************************************************************************/
#define BEGINNING_OF_FRAME_SIZE	sizeof(struct exch_proto_frame)
#define END_OF_FRAME_SIZE		sizeof(uint8_t[2])

#define FULL_FRAME_LEN_DIFF		(BEGINNING_OF_FRAME_SIZE + END_OF_FRAME_SIZE)
#define MIN_FRAME_LEN			(BEGINNING_OF_FRAME_SIZE + END_OF_FRAME_SIZE)

/* Receive */
#define BUF_SIZE		(2048)
static uint8_t rcv_buf[BUF_SIZE];
static unsigned int rcv_idx;

/* Send */
static struct comm_task {
	struct elem *q_next;	 
	uint8_t *buf;
	uint16_t size;
	EXCH_PROTO_CB cb;
} *curr_task;

static int tx_idx;
static int tx_remaining;
static struct queue tx_queue;

/******************************************************************************/
/*				Frame Functions				      */
/******************************************************************************/
#define IS_HEADER_VALID(frame) \
	((frame)->sync_ch[0] == EXCH_PROTO_SYNC_CH_1 && (frame)->sync_ch[1] == EXCH_PROTO_SYNC_CH_2)

#if 0
static inline void calc_crc(struct exch_proto_frame *frame, uint8_t ck[])
{
	int i, len;
	uint8_t *buf = &frame->type;

	ck[0] = 0;
	ck[1] = 0;

	len = frame->len + sizeof(frame->msg_id) + sizeof(frame->class_id) + sizeof(frame->type);
	
	for (i = 0; i < len; i++) {		
		ck[0] = (uint16_t)(ck[0] + buf[i]) % 0xFF;
		ck[1] = (uint16_t)(ck[1] + ck[0]) % 0xFF;
	}
}
#endif

static int is_frame_valid(struct exch_proto_frame *frame)
{
	uint8_t ck[2];
	uint8_t fr_ck[2];
	
	unsigned int crc_len = frame->len + sizeof(frame->msg_id) + sizeof(frame->class_id) + sizeof(frame->type);
	uint8_t *crc_data = (uint8_t *)&frame->type;
	
	calc_crc(&crc_data[0], crc_len, &ck[0]);
	fr_ck[0] = frame->payload[frame->len];
	fr_ck[1] = frame->payload[frame->len + 1];
	if (ck[0] == fr_ck[0] && ck[1] == fr_ck[1])
		return 1;
	else
		return 0;
}

static inline int get_frame_len(struct exch_proto_frame *frame)
{
	return frame->len + FULL_FRAME_LEN_DIFF;
}

/******************************************************************************/
/*						ECU Comm Write/Read									  */
/******************************************************************************/
static void clear_tx_data(void)
{	
	free(curr_task->buf);
	free(curr_task);	
	curr_task = NULL;

#if 0
	tx_remaining = 0;
#endif
}

/*
 * Deletes and realigns the data until it encounters a valid start of a
 * new message
 */
static void drop_bytes(uint16_t bytes_to_discard)
{
	int i = 0;

	/*
	 * If the first bytes are garbage, then we need to process them one by
	 * one until we get to the header of the next valid message
	 */
	if (!bytes_to_discard) {
		for (i = 0; i < rcv_idx; i++) {

			if (rcv_buf[i] != EXCH_PROTO_SYNC_CH_1)
				continue;

			if (i == (rcv_idx - 1) || rcv_buf[i + 1] == EXCH_PROTO_SYNC_CH_2)
				break;
		}

		if (i > rcv_idx)
			i = rcv_idx;
		
		memmove(&rcv_buf[0], &rcv_buf[i], rcv_idx - i);
		memset(&rcv_buf[rcv_idx - i], 0, i);
		rcv_idx -= i;
		
				

	/* If we want to discard a valid message to get to the next */
	} else {
		if (bytes_to_discard > rcv_idx)
			bytes_to_discard = rcv_idx;

		/* Discard the entire message based on its length */
		memmove(&rcv_buf[0], &rcv_buf[bytes_to_discard], rcv_idx - bytes_to_discard);
		rcv_idx -= bytes_to_discard;
	}
}

static void exch_proto_read(void)
{
#ifdef USB_PROTO
	int bytes_read = 0, bytes_to_read = 0;
#else
	unsigned short bytes_read = 0, bytes_to_read = 0;
#endif
	
	int bytes_in_buf;
	struct exch_proto_frame *frame;	

	/* Read data in buffer */
#ifdef USB_PROTO
	do { 
		bytes_to_read = (CDC1_GetCharsInRxBuf() > (BUF_SIZE - rcv_idx)) ? (BUF_SIZE - rcv_idx) : CDC1_GetCharsInRxBuf();			
		while (bytes_to_read)
			if (CDC1_RecvChar(&rcv_buf[rcv_idx + bytes_read]) != ERR_FAILED) {
				bytes_to_read--;
				bytes_read++;
			} else {
				break;			
			}
		
		bytes_in_buf = CDC1_GetCharsInRxBuf();
		
	} while (bytes_in_buf && (BUF_SIZE - rcv_idx));
#else
	int status;
	do {
		status = UART_Comms_RecvBlock(&rcv_buf[rcv_idx + bytes_read], BUF_SIZE - rcv_idx, &bytes_to_read);		
		bytes_in_buf = UART_Comms_GetCharsInRxBuf();
		MY_ASSERT(status == ERR_OK || status == ERR_COMMON || status == ERR_RXEMPTY);
		bytes_read += bytes_to_read;
		
#if 0
		memcpy(&dbg_struct[idx].buf[0], &rcv_buf[rcv_idx], bytes_read);
		dbg_struct[idx].size = bytes_read;
		if (bytes_read)
			idx = (idx + 1) % 100;
#endif
	} while (bytes_in_buf && (BUF_SIZE - rcv_idx) > 0);
#endif
	
	if (bytes_read <= 0 && !rcv_idx) {
		return;
	}
	
	rcv_idx += bytes_read;
		
	/* Discard garbage data from the buffer */
	if (!IS_HEADER_VALID((struct exch_proto_frame *)&rcv_buf[0]))
		drop_bytes(0);

	/* If we don't have the entire frame in buffer, return */
	frame = (struct exch_proto_frame *)&rcv_buf[0];
	while (1) {
		if (rcv_idx < MIN_FRAME_LEN || get_frame_len(frame) > rcv_idx) {			
			break;
		}

		if (!is_frame_valid(frame)) {			
			drop_bytes(get_frame_len(frame));			
			continue;
		}
		
		switch (frame->class_id) {
		case EXCH_PROTO_CLASS_SYS:
			exch_proto_sys_handler(frame->type, frame->msg_id, &frame->payload[0], frame->len);
			break;
		case EXCH_PROTO_CLASS_TABLES:
			exch_proto_tables_handler(frame->type, frame->msg_id, &frame->payload[0], frame->len);
			break;
		case EXCH_PROTO_CLASS_DRIVERS:
			exch_proto_drivers_handler(frame->type, frame->msg_id, &frame->payload[0], frame->len);			
			break;
		case EXCH_PROTO_CLASS_REPORT:
			/* TODO: If report handling suddenly doesn't work, check this */
			exch_proto_report_handler(frame->type, frame->msg_id, &frame->payload[0], frame->len);
			break;
		case EXCH_PROTO_CLASS_FW_UPDATE:
			exch_proto_fw_upd_handler(frame->type, frame->msg_id, &frame->payload[0], frame->len);
			break;
		case EXCH_PROTO_CLASS_DATALOG:
			exch_proto_datalog_handler(frame->type, frame->msg_id, &frame->payload[0], frame->len);
			break;
		default:
			break;
		}
		drop_bytes(get_frame_len(frame));		
	}	
}

volatile int test_val = -1;

static void exch_proto_write(void)
{
	int bytes_to_write, tx_avail, txed = 0;

	do {
		/* Load the number of bytes available in the UART buffer */
#ifdef USB_PROTO
		tx_avail = CDC1_GetFreeInTxBuf();
#else
		tx_avail = UART_Comms_OUT_BUF_SIZE - UART_Comms_GetCharsInTxBuf();
#endif

		/* Bytes available from transmission */
		if (tx_remaining) {
			if (!tx_avail)
				return;

			bytes_to_write = (tx_avail > tx_remaining) ?
				tx_remaining : tx_avail;

			/* Put data in UART driver's buffer */			
#ifdef USB_PROTO
			txed = bytes_to_write;
			while (bytes_to_write && CDC1_SendChar(curr_task->buf[tx_idx]) == ERR_OK) {
				tx_idx++;
				bytes_to_write--;
			}
			txed -= bytes_to_write;
			tx_remaining -= txed;
			bytes_to_write = 0;
#else
			txed = 0;
			UART_Comms_SendBlock(&curr_task->buf[tx_idx], bytes_to_write, &txed);
			tx_idx += txed;
			tx_remaining -= txed;		
#endif			

			if (!tx_remaining) {
				if (curr_task->cb)
					curr_task->cb(EXCH_PROTO_SUCCESS);
				clear_tx_data();
			}
		}
		
		if (!curr_task) {
			curr_task = (struct comm_task*) queue_pop(&tx_queue);			
			/* Empty queue */
			if (!curr_task)
				return;
						
			if (curr_task->size == 0)
				test_val = -1;
			tx_remaining = curr_task->size;
			tx_idx = 0;
		}
	} while (curr_task && tx_avail != 0);
}

void exch_proto_tick(void)
{
	/* call the periodic task function */
#ifdef USB_PROTO
	USB_Class_CDC_Periodic_Task();
#endif
	
	exch_proto_read();
	exch_proto_write();
	
#ifdef USB_PROTO
	/* call the periodic task function */
	USB_Class_CDC_Periodic_Task();
#endif
}

/* High-level access functions */
void *exch_proto_alloc_buf(uint16_t size)
{		
	void *tmp;
	//Cpu_DisableInt();
	if (size == 1161)
		tmp = NULL;
	tmp = malloc(size + FULL_FRAME_LEN_DIFF);
	//Cpu_EnableInt();
	MY_ASSERT(tmp != NULL);	
	return (tmp + BEGINNING_OF_FRAME_SIZE);
}

int exch_proto_enqueue(uint8_t type, uint8_t class_id, uint8_t msg_id, uint16_t payload_size, uint8_t payload[], EXCH_PROTO_CB my_cb)
{
	uint32_t crc_len;
	uint8_t *crc_data;	
	struct exch_proto_frame *frame = NULL;

	//Cpu_DisableInt();
	struct comm_task *task = (struct comm_task *) malloc(sizeof(struct comm_task));
	//Cpu_EnableInt();
	MY_ASSERT(task != NULL);
	if (!task)
		return -EXIT_FAILURE;

	task->buf = &payload[0] - BEGINNING_OF_FRAME_SIZE;

	frame = (struct exch_proto_frame *) &task->buf[0];
	frame->sync_ch[0] = EXCH_PROTO_SYNC_CH_1;
	frame->sync_ch[1] = EXCH_PROTO_SYNC_CH_2;
	frame->len = payload_size;
	frame->type = type;
	frame->class_id = class_id;
	frame->msg_id = msg_id;
	
	crc_len = frame->len + sizeof(frame->msg_id) + sizeof(frame->class_id) + sizeof(frame->type);
	crc_data = (uint8_t *)&frame->type;
	calc_crc(&crc_data[0], crc_len, &frame->payload[payload_size]);

	task->size = get_frame_len(frame);
	task->cb = my_cb;

	queue_push(&tx_queue, task);

	return 0;
}

int exch_proto_init(void)
{
	/* Init USB */	
#ifdef USB_PROTO
#else
	UART_Comms_Init();
#endif
	
	return 0;
}
