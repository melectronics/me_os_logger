/*
 * proto.h
 *
 *  Created on: Feb 27, 2014
 *      Author: John
 */

#ifndef PROTO_H_
#define PROTO_H_

#include <main.h>


/******************************************************************************/
//                      High Level Protocol
/******************************************************************************/
enum exch_proto_class_id_t {
	EXCH_PROTO_CLASS_REPORT,
	EXCH_PROTO_CLASS_TABLES,
	EXCH_PROTO_CLASS_DRIVERS,
	EXCH_PROTO_CLASS_DATA_LINKS,
	EXCH_PROTO_CLASS_SYS,
	EXCH_PROTO_CLASS_FW_UPDATE,
	EXCH_PROTO_CLASS_DATALOG,
};





/******************************************************************************/
/*                  Access layer                                              */
/******************************************************************************/
enum exch_proto_msg_type_t {
	EXCH_PROTO_MSG_REQ = 0x0,
	EXCH_PROTO_MSG_RSP = 0xF
};

/* FIXME: While working with this, misaligned memory accesses may be generated */
struct exch_proto_frame {
    uint8_t sync_ch[2];
    uint16_t len;
    uint8_t type;
    uint8_t class_id;
    uint8_t msg_id;
    uint8_t payload[0];
} __attribute__((packed));

/* Protocol key bytes */
#define EXCH_PROTO_SYNC_CH_1     ('M')
#define EXCH_PROTO_SYNC_CH_2     ('E')

enum exch_proto_status_t {
    EXCH_PROTO_SUCCESS,
    EXCH_PROTO_FAILURE,
    EXCH_PROTO_INVALID_PARAM,
    EXCH_PROTO_UNSUPP_REQ,
    EXCH_PROTO_INSUFF_MEM,
    EXCH_PROTO_FAILED_CRC,
    EXCH_PROTO_FAILED_FLASH_WRITE,
    EXCH_PROTO_BUSY
};

typedef void (*EXCH_PROTO_CB)(int status);

/* Tick-functions, called for each loop iteration */
void exch_proto_tick(void);

/* High-level access functions */
void *exch_proto_alloc_buf(uint16_t size);
int exch_proto_enqueue(uint8_t type, uint8_t class_id, uint8_t msg_id, uint16_t payload_size, uint8_t payload[], EXCH_PROTO_CB my_cb);
int exch_proto_init(void);

#endif /* PROTO_H_ */
