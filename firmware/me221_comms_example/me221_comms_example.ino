#include <SoftwareSerial.h>

#define DEBUG                             (1)

/**************************************************/
/*      ECU Comms                                 */
/**************************************************/
#define MIN_FRAME_LEN                     (10)
#define MAX_FRAME_LEN                     (768)
#define SYNC_CH_0                         ('M')
#define SYNC_CH_1                         ('E')

#define MSG_TYPE_REQ                      (0x0)
#define MSG_TYPE_RSP                      (0xF)

#define CLASS_ID_REPORT                   (0x0)

#define MSG_ID_REPORT                     (0x0)
#define MSG_ID_ACK                        (0x1)
#define MSG_ID_SET_STATE                  (0x2)

byte rx_buf[MAX_FRAME_LEN];
int rx_idx;
unsigned int rx_frame_len;

struct proto_frame {
  byte sync_ch[2];
  unsigned int len;
  byte type;
  byte class_id;
  byte msg_id;
};

SoftwareSerial Debug_Serial(10, 11); //232_TX,232_RX // Config to assign Digital io ports for the RS232 Shield
struct proto_frame rx_frame;

void start_reporting_req() {
  byte req [] = { 0x4D, 0x45, 0x01, 0x00, 0x00, 0x00, 0x02, 0x01, 0x03, 0x05, };
  Debug_Serial.print("Sending: ");
  Debug_Serial.println(sizeof(req));
  Serial.write(req, sizeof(req));
}

void setup() {
  /* Init debug serial */
  Debug_Serial.begin(57600);
  Debug_Serial.println();
  Debug_Serial.println("Serial debug line open");

  /* Init ECU comms */
  Serial.begin(115200);
  while (!Serial); // wait for Software serial port to connect. Needed for Leonardo only 
  rx_idx = 0;  
  delay(2000);

  Debug_Serial.println("Initialized software serial");

  start_reporting_req();
}

void process_frame(int payload_offset, byte type, byte class_id, byte msg_id, unsigned int payload_len)
{
  if (class_id != CLASS_ID_REPORT)
    return;

  switch (msg_id) {
    case MSG_ID_REPORT:
    Debug_Serial.println("Received a report");
    break;
    case MSG_ID_SET_STATE:
    Debug_Serial.println("Received a set state response");
    break;
  }
    
}

void loop() {
  while (Serial.available() && rx_idx < MAX_FRAME_LEN) {
    rx_buf[rx_idx++] = Serial.read();
  }

  Debug_Serial.print("Received ");
  Debug_Serial.println(rx_idx);

  for (int i = 0; i < (rx_idx - 1); i++) {
    
#if (DEBUG == 1)
  Debug_Serial.print(rx_buf[i], HEX);
  Debug_Serial.print(" ");
#endif
  

    if (rx_buf[i] == SYNC_CH_0 && rx_buf[i + 1] == SYNC_CH_1) {     
      /* Confirm that we've received the entire message */

      Debug_Serial.print("SoF found at ");
      Debug_Serial.println(i);
      
      memcpy(&rx_frame_len, &rx_buf[i + 2], 2);
      if (rx_frame_len <= (rx_idx - i + 2)) {        
        process_frame(i + 7, rx_buf[i + 4], rx_buf[i + 5], rx_buf[i + 6], rx_frame_len);
      }

      /* Reset receive buffer */
      rx_idx = 0;
    }   
  }
  Debug_Serial.println("\n");
}
